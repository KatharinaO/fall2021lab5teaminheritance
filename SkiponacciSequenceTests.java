import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class SkiponacciSequenceTests 
{
    @Test
    public void testConstructor() 
    {
        Sequence seq = new SkiponacciSequence(0, 0, 0);
        assertEquals(0, seq.getTerm(0));
        assertEquals(0, seq.getTerm(1));
        assertEquals(0, seq.getTerm(2));

        seq = new SkiponacciSequence(1, 2, 3);
        assertEquals(1, seq.getTerm(0));
        assertEquals(2, seq.getTerm(1));
        assertEquals(3, seq.getTerm(2));

        seq = new SkiponacciSequence(-120, 6, 17);
        assertEquals(-120, seq.getTerm(0));
        assertEquals(6, seq.getTerm(1));
        assertEquals(17, seq.getTerm(2));
    }

    @Test
    public void testSequence1()
    {
        Sequence seq = new SkiponacciSequence(2, 4, 1);
        assertEquals(2, seq.getTerm(0));
        assertEquals(4, seq.getTerm(1));
        assertEquals(1, seq.getTerm(2));
        assertEquals(6, seq.getTerm(3));
        assertEquals(5, seq.getTerm(4));
        assertEquals(7, seq.getTerm(5));
        assertEquals(11, seq.getTerm(6));
        assertEquals(12, seq.getTerm(7));
        assertEquals(18, seq.getTerm(8));
        assertEquals(23, seq.getTerm(9));
    }

    @Test
    public void testSequence2()
    {
        Sequence seq = new SkiponacciSequence(7, -2, 5);
        assertEquals(7, seq.getTerm(0));
        assertEquals(-2, seq.getTerm(1));
        assertEquals(5, seq.getTerm(2));
        assertEquals(5, seq.getTerm(3));
        assertEquals(3, seq.getTerm(4));
        assertEquals(10, seq.getTerm(5));
        assertEquals(8, seq.getTerm(6));
        assertEquals(13, seq.getTerm(7));
        assertEquals(18, seq.getTerm(8));
        assertEquals(21, seq.getTerm(9));
    }

    @Test
    public void testValidation() 
    {
        Sequence seq = new SkiponacciSequence(7, -2, 5);
        try {
            assertEquals(7, seq.getTerm(-5));
            fail("No exception thrown when getting negative term");
        } catch (Exception e) {}
        try {
            assertEquals(7, seq.getTerm(-1));
            fail("No exception thrown when getting negative term");
        } catch (Exception e) {}
    }
}
