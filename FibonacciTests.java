import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class FibonacciTests {
    
    @Test
    //test if constructor returns right values
    public void testConstructor() {
        FibonacciSequence f = new FibonacciSequence(2, 4);
        assertEquals(2, f.getTerm(0));
        assertEquals(4, f.getTerm(1)); 
    }

    @Test
    //test if getTerm returns value from right position
    public void testGetTerm() {
        FibonacciSequence f = new FibonacciSequence(2, 4);
        assertEquals(4, f.getTerm(1));
    }

    @Test
    //test the validation of the getTerm method
    public void testValidation() {
        FibonacciSequence f = new FibonacciSequence(2, -4);
        try {
            assertEquals(7, f.getTerm(-8));
            fail("No exception thrown when getting negative term");
        } catch (Exception e) {}
        try {
            assertEquals(7, f.getTerm(-2));
            fail("No exception thrown when getting negative term");
        } catch (Exception e) {}
    }

}
