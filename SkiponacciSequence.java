public class SkiponacciSequence extends Sequence{

    private int x;
    private int y;
    private int z;

    public  SkiponacciSequence(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getTerm(int n) {
        if(n < 0){
            throw new IllegalArgumentException("Negative Numbers are Invalid");
        }
        int[] skipSequence;
        if(n <= 3){
            skipSequence = new int[3];
        } else {
            skipSequence = new int[n+1];
        }

        skipSequence[0] = this.x;
        skipSequence[1] = this.y;
        skipSequence[2] = this.z;
        for (int i = 2; i < n; i++) {
            skipSequence[i] = skipSequence[i - 2] + skipSequence[i - 1];
        }
        return skipSequence[n-1];
    }
}