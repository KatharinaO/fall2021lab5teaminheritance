public class FibonacciSequence extends Sequence
{
    private int TermOne;
    private int TermTwo;

    public FibonacciSequence(int first, int second)
    {
        this.TermOne = first;
        this.TermTwo = second;
    }

    public int getTerm(int number)
    {  
        if(number < 0) {throw new IllegalArgumentException("negative index");};
        if(number == 0) {return this.TermOne;}
        if(number == 1) {return this.TermTwo;}


        int[] array = new int[number+1];

        array[0] = this.TermOne;
        array[1] = this.TermTwo;
    
        
        for (int i = 2; i < array.length; i++)
        {
            array[i] = array[i-1] + array[i-2];
        }
        return array[number-1];
    }
    
}
