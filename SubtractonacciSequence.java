public class SubtractonacciSequence extends Sequence 
{
    int term1, term2;
    public SubtractonacciSequence (int term1, int term2)
    {
        super();
        this.term1 = term1;
        this.term2 = term2;
    }

    public int getTerm(int n) 
    {
        if(n < 0) { throw new IllegalArgumentException("Cannot get a negative term"); }
        if(n == 0) { return this.term1; }
        if(n == 1) { return this.term2; }

        int[] arr = new int[n + 1];
        
        arr[0] = this.term1;
        arr[1] = this.term2;

        for(int i = 2; i < arr.length; i++){
            arr[i] = arr[i - 2] - arr[i - 1];
        }

        return arr[n];
    }
}