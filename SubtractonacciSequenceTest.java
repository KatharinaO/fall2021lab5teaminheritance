import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

// the test sequence is 5 - 2 - 3 - -1 - 4 - -5 
public class SubtractonacciSequenceTest 
{
    SubtractonacciSequence test = new SubtractonacciSequence(5, 2);
    @Test
    public void testConstructorTerms() {
        assertEquals(5,test.getTerm(0));
        assertEquals(2,test.getTerm(1));
    }

    @Test
    public void testGetTerm() {
        assertEquals(3,test.getTerm(2));
        assertEquals(-1,test.getTerm(3));
        assertEquals(4,test.getTerm(4));
        assertEquals(-5,test.getTerm(5));
    }
}
