import java.util.Scanner;

public class Application 
{


    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a valid sequence string");
        String inputSeq = scan.next();
        scan.close();

        if(validate(inputSeq)) { 
            for(int i = 1; i < 10; i++) {
                print(parse(inputSeq), i);
            }
        } else{
            System.out.println("Invalid string please try again thanks ;)");
        }

    }
    
    public static void print(Sequence[] sequences, int number)
    {
        for(int i = 0; i < sequences.length; i++)
        {
            System.out.println(sequences[i].getTerm(number));
        }
    }
        
    static Sequence[] parse(String str) {
        String[] splitStr = str.split(";");
        int length = splitStr.length / 4;
        Sequence[] seqs = new Sequence[length];

        for(int i = 0; i < splitStr.length; i += 4){
            if(splitStr[i].equals("Fib")) {
                seqs[i/4] = new FibonacciSequence(
                    Integer.parseInt(splitStr[i + 1]), 
                    Integer.parseInt(splitStr[i + 2])
                );
            }
            else if(splitStr[i].equals("Sub")) {
                seqs[i/4] = new SubtractonacciSequence(
                    Integer.parseInt(splitStr[i + 1]), 
                    Integer.parseInt(splitStr[i + 2])
                );
            }
            else if(splitStr[i].equals("Skip")) {
                seqs[i/4] = new SkiponacciSequence(
                    Integer.parseInt(splitStr[i + 1]),
                    Integer.parseInt(splitStr[i + 2]), 
                    Integer.parseInt(splitStr[i + 3])
                );
            }
        }
        return seqs;
    }

    public static boolean validate(String s){
       String[] splitS =  s.split(";");
       if (splitS.length % 4 != 0) {
           return false;
       }
       for (int i = 0; i < splitS.length; i += 4) {
           if (!(splitS[i].equals("Fib") || splitS[i].equals("Skip") || splitS[i].equals("Sub"))) {
               return false;
           }
           if (splitS[i].equals("Skip")) {
            try {
                Integer.parseInt(splitS[i+1]);
                Integer.parseInt(splitS[i+2]);
                Integer.parseInt(splitS[i+3]);
               } catch (NumberFormatException e) {
                    return false;
               }
           } else {
            try {
                Integer.parseInt(splitS[i+1]);
                Integer.parseInt(splitS[i+2]);
            } catch (NumberFormatException e) {
                return false;
            }
            if(!splitS[i+3].equals("x")) {
                return false;
            }
           }
       }
       return true;
    }
}
